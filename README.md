__The Movie Database__ : Aplicación de evaluación técnica

__Técnico__\
Lenguaje Swift 5\
SWIFT UI\
Swift Combine\
Test Unitarios\
Swift Package Manager

__Por decidir__\
Core Data

__Arquitectura__\
Principios SOLID y Patrones de Diseño

__Por decidir__\
Clean Architecture + MVVM, VIPER o Lottus

__Bibliotecas__
|Nombre|Objetivo|
|---|---|
|.|.|


